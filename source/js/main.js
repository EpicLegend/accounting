'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/carousel.js
//= library/wow.js
//= library/jquery-ui.js


$(document).ready(function () {


	// Первый калькулятор
	$(".calc__form").submit(function (e) {
		e.stopPropagation();
		e.preventDefault();

		var amount = $(this).find("input[name=amount]").val();
		//var scb = 12;
		var kd1 = new Date( $(this).find("input[name=firstDate]").val() );
		var kd2 = new Date( $(this).find("input[name=secondDate]").val() );

		var millisec = +kd2 - ( +kd1 );
		var days = (millisec / (1000 * 60 * 60 * 24)).toFixed();
		var limitDays = 0;

		var result = 0;

		// просрочка
		console.log("q" +days);
		limitDays = days - 30;
		if ( limitDays < 0 ) {
			limitDays = 0;
			
			console.log("+" + days);
		} else {
			days = 30;
		}

		result = ( ( amount * (1 / 300) )  * days ) + ( (amount * (1 / 150) ) * limitDays );
		result = result.toFixed();

		$(this).find("#result").html("( "+amount+" * (1 / 300)  * "+ days +") + ( ("+amount+" * (1 / 150) ) * "+limitDays+" ) = " + result);
	});

	// form_ndc
	$(".btn_form_ndc").click(function (e) {
		var that = $(this).parent().parent();

		e.stopPropagation();
		e.preventDefault();

		var digets = $(that).find("input[name=digets]").val();

		var ndc = $(that).find("select[name=precent]").val();
		ndc = parseInt(ndc) / 100 ;

		var operand = $(this).attr("data-type");
		var result = 0;

		if ( operand == "+" ) {
			result = parseInt(digets) + parseInt( (ndc * digets).toFixed() );

			$("#form_ndc_first_p").html("Сумма без НДС");
			$("#form_ndc_first").html(digets);

			$("#form_ndc_second_p").html("НДС " + (ndc * 100) + "%" );
			$("#form_ndc_second").html( (digets * ndc).toFixed() );

			$("#form_ndc_third_p").html("Сумма с НДС");
			$("#form_ndc_third").html(result.toFixed(2));
		} else if (operand == "-"){
			result = ( ( ( digets / ( 1 + ndc ) ) - digets ) * -1 ).toFixed(2);

			$("#form_ndc_first_p").html("Сумма с НДС");
			$("#form_ndc_first").html(digets);

			$("#form_ndc_second_p").html("НДС %" );
			$("#form_ndc_second").html( result );

			$("#form_ndc_third_p").html("Сумма без НДС ");
			$("#form_ndc_third").html( (digets - result).toFixed(2) );
		}
	});

	$("input[name=precenttwo]").change(function () {
		var active =  parseInt( $(this).val() );

		if ( active == 6 ) {
			$(".res_table_first").addClass("active");
			$(".input_res_table_second").addClass("active");

			$(".input_res_table_first").removeClass("active");
		} else if ( active == 15 ) {
			$(".input_res_table_second").removeClass("active");
			$(".input_res_table_first").addClass("active");
		}
	});

	$("input[name=quarter]").change(function (e) {
		var value = $(this).val();
		var items = $(".row__kv");

		if ( value == 3 ) {
			items.removeClass("active");
			items.attr("disabled");
			$( items[0] ).addClass("active");
			$( items[0] ).find("input").removeAttr("disabled");
		} else if (value == 6) {
			items.removeClass("active");
			items.attr("disabled");
			$( items[0] ).addClass("active");
			$( items[0] ).find("input").removeAttr("disabled");
			$( items[1] ).addClass("active");
			$( items[1] ).find("input").removeAttr("disabled");
		} else if (value == 9) {
			items.removeClass("active");
			items.attr("disabled");
			$( items[0] ).addClass("active");
			$( items[0] ).find("input").removeAttr("disabled");
			$( items[1] ).addClass("active");
			$( items[1] ).find("input").removeAttr("disabled");
			$( items[2] ).addClass("active");
			$( items[2] ).find("input").removeAttr("disabled");
		} else if (value == 12) {
			items.removeClass("active");
			items.attr("disabled");
			$( items[0] ).addClass("active");
			$( items[0] ).find("input").removeAttr("disabled");
			$( items[1] ).addClass("active");
			$( items[1] ).find("input").removeAttr("disabled");
			$( items[2] ).addClass("active");
			$( items[2] ).find("input").removeAttr("disabled");
			$( items[3] ).addClass("active");
			$( items[3] ).find("input").removeAttr("disabled");
		}


	});
	// form_ycn

	function WhoAreYou (value, drop, obj) {
		console.log("value--" + value / 2);
		console.log("drop--" + drop);
		console.log("obj--" + obj);
		if ( obj > 0) {
			if ( drop > (value / 2) ) {
				console.log("1/2");
				return ( value / 2 );
			} 
		return value - drop;
		} 
		return value - drop;
	}

	$(".form_ycn").submit(function (e) {
		e.stopPropagation();
		e.preventDefault();

		var result = 0;
		var whoAreYou = $(this).find("input[name=whoareyou]:checked").val(); 

		var precent = $( this ).find("input[name=precenttwo]:checked").val();
		precent = precent / 100;
		var count = $(this).find("input[name=quarter]:checked").val();
		count = count / 3;

		// Вообще это проверка на пустые поля
		var items = $(".res_table input");
		for ( var i = 0; i < count * 3; i++) {
			if ( $(items[i]).val() == "" ) {
				console.log("error");
				// надо бы обработать ошибку, но как-то потом
			}
		}


		//


		// определить количество %
		if ( precent == 0.06 ) {
			// Перебор строк с инпутами
			var meResult
			switch (count) {
				case 1:
					meResult = parseInt( $("input[name=kv1_1]").val() ) * precent - parseInt( $("input[name=kv1_2]").val() );
					result = meResult;

					var a = parseInt( $("input[name=kv1_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv1_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv1_3]").val( meResult );

					break;
				case 2:
					meResult = parseInt( $("input[name=kv1_1]").val() ) * precent - parseInt( $("input[name=kv1_2]").val() );
					result = meResult;

					var a = parseInt( $("input[name=kv1_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv1_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv1_3]").val( meResult );

					meResult = parseInt( $("input[name=kv2_1]").val() ) * precent - parseInt( $("input[name=kv2_2]").val() );
					result += meResult;

					var a = parseInt( $("input[name=kv2_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv2_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv2_3]").val( meResult );

					break;
				case 3:
					meResult = parseInt( $("input[name=kv1_1]").val() ) * precent - parseInt( $("input[name=kv1_2]").val() );
					result = meResult;

					var a = parseInt( $("input[name=kv1_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv1_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv1_3]").val( meResult );

					meResult = parseInt( $("input[name=kv2_1]").val() ) * precent - parseInt( $("input[name=kv2_2]").val() );
					result += meResult;

					var a = parseInt( $("input[name=kv2_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv2_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv2_3]").val( meResult );

					meResult = parseInt( $("input[name=kv3_1]").val() ) * precent - parseInt( $("input[name=kv3_2]").val() );
					result += meResult;

					var a = parseInt( $("input[name=kv3_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv3_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv3_3]").val( meResult );

					break;
				case 4:
					meResult = parseInt( $("input[name=kv1_1]").val() ) * precent - parseInt( $("input[name=kv1_2]").val() );
					result = meResult;

					var a = parseInt( $("input[name=kv1_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv1_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv1_3]").val( meResult );

					meResult = parseInt( $("input[name=kv2_1]").val() ) * precent - parseInt( $("input[name=kv2_2]").val() );
					result += meResult;

					var a = parseInt( $("input[name=kv21]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv2_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv2_3]").val( meResult );

					meResult = parseInt( $("input[name=kv3_1]").val() ) * precent - parseInt( $("input[name=kv3_2]").val() );
					result += meResult;


					var a = parseInt( $("input[name=kv3_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv3_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv3_3]").val( meResult );

					meResult = parseInt( $("input[name=kv4_1]").val() ) * precent - parseInt( $("input[name=kv4_2]").val() );
					result += meResult;


					var a = parseInt( $("input[name=kv4_1]").val() ) * precent;
					meResult = WhoAreYou( a, parseInt( $("input[name=kv4_2]").val() ) , whoAreYou );

					$( this ).find("input[name=kv4_3]").val( meResult );

					break;
				default:
					console.log("Хули надо здесь?");
					break;
			}
		} else if (precent == 0.15) {
			// Перебор строк с инпутами 
			var meResult
			switch (count) {
				case 1:
					meResult = parseInt( $("input[name=kv1_1]").val() ) * precent -  parseInt( $("input[name=kv1_2]").val() ) - parseInt( $("input[name=kv_spe_1]").val() );
					result = meResult;
					$( this ).find("input[name=kv1_3]").val( meResult );

					break;
				case 2:
					meResult = parseInt( $("input[name=kv1_1]").val() ) * precent - parseInt( $("input[name=kv1_2]").val() ) - parseInt( $("input[name=kv_spe_1]").val() );
					result = meResult;
					$( this ).find("input[name=kv1_3]").val( meResult );

					meResult = parseInt( $("input[name=kv2_1]").val() ) * precent - parseInt( $("input[name=kv2_2]").val() ) - parseInt( $("input[name=kv_spe_2]").val() );
					result += meResult;
					$( this ).find("input[name=kv2_3]").val( meResult );

					break;
				case 3:
					meResult = parseInt( $("input[name=kv1_1]").val() ) * precent - parseInt( $("input[name=kv1_2]").val() ) - parseInt( $("input[name=kv_spe_1]").val() );
					result = meResult;
					$( this ).find("input[name=kv1_3]").val( meResult );

					meResult = parseInt( $("input[name=kv2_1]").val() ) * precent - parseInt( $("input[name=kv2_2]").val() ) - parseInt( $("input[name=kv_spe_2]").val() );
					result += meResult;
					$( this ).find("input[name=kv2_3]").val( meResult );

					meResult = parseInt( $("input[name=kv3_1]").val() ) * precent - parseInt( $("input[name=kv3_2]").val() ) - parseInt( $("input[name=kv_spe_3]").val() );
					result += meResult;
					$( this ).find("input[name=kv3_3]").val( meResult );

					break;
				case 4:
					meResult = parseInt( $("input[name=kv1_1]").val() ) * precent - parseInt( $("input[name=kv1_2]").val() ) - parseInt( $("input[name=kv_spe_1]").val() );
					result = meResult;
					$( this ).find("input[name=kv1_3]").val( meResult );

					meResult = parseInt( $("input[name=kv2_1]").val() ) * precent - parseInt( $("input[name=kv2_2]").val() ) - parseInt( $("input[name=kv_spe_2]").val() );
					result += meResult;
					$( this ).find("input[name=kv2_3]").val( meResult );

					meResult = parseInt( $("input[name=kv3_1]").val() ) * precent - parseInt( $("input[name=kv3_2]").val() ) - parseInt( $("input[name=kv_spe_3]").val() );
					result += meResult;
					$( this ).find("input[name=kv3_3]").val( meResult );

					meResult = parseInt( $("input[name=kv4_1]").val() ) * precent - parseInt( $("input[name=kv4_2]").val() ) - parseInt( $("input[name=kv_spe_4]").val() );
					result += meResult;
					$( this ).find("input[name=kv4_3]").val( meResult );

					break;
				default:
					console.log("Хули надо здесь?");
					break;
			}
		}

		var items = $(".row__kv");

		$(".result").html(result);
	});

var arr = [0, 1400, 2800, 5800, 8800];
$(".form_ndfl input[name=ndfl], .form_ndfl input[name=summa], .form_ndfl input[name=notNdfl]").on("keyup input", function () {
	var ndfl 		= parseInt( $(".form_ndfl input[name=ndfl]").val() );
	var summa 		= parseInt( $(".form_ndfl input[name=summa]").val() );
	var notNdfl 	= parseInt( $(".form_ndfl input[name=notNdfl]").val() );

	if ( ndfl < 0 && summa < 0 && notNdfl < 0) {
		$(".form_ndfl input[name=ndfl]").val("");
		$(".form_ndfl input[name=summa]").val("");
		$(".form_ndfl input[name=notNdfl]").val("");
	} else {
		if ( $(this).attr("name") == "ndfl" ) {
			$(".form_ndfl").find("input[name=summa]").val( calc('summa') );
			$(".form_ndfl").find("input[name=notNdfl]").val( calc('notNdfl') );
		} else if ( $(this).attr("name") == "summa" ) {

			$(".form_ndfl").find("input[name=ndfl]").val( calc('ndfl') );
			$(".form_ndfl").find("input[name=notNdfl]").val( calc('notNdfl') );

		} else if ( $(this).attr("name") == "notNdfl" ) {
			$(".form_ndfl").find("input[name=summa]").val( 0 );

			$(".form_ndfl").find("input[name=ndfl]").val( calc("ndfl") );
			$(".form_ndfl").find("input[name=summa]").val( calc('summa') );

		} else if ( $(this).attr(name) == "countChild" ) {

			console.log("countChild");

		}
	}

	
} );

function calc(action) {
	var precent 	= $(".form_ndfl").find("input[name=tax]:checked").val();
	precent 		= precent / 100;

	var ndfl 		= parseInt( $(".form_ndfl input[name=ndfl]").val() );
	if ( ndfl == '' ) ndfl = 0;

	var summa 		= parseInt( $(".form_ndfl input[name=summa]").val() );
	if ( summa == '' ) summa = 0;

	var notNdfl 	= parseInt( $(".form_ndfl input[name=notNdfl]").val() );
	if ( notNdfl == '' ) notNdfl = 0;

	var countChild 	= $(".form_ndfl").find("select[name=countChild]").val();
	var valueChild 	= arr[countChild];
	var result 		= 0;

	if ( action == "summa") {

		return calcSumma();

	} else if ( action == "notNdfl" ) {

		return calcNotNdfl();

	} else if ( action == "ndfl" ) {

		return calcNdfl();

	}

	function calcNdfl() {
		console.log("summa = " + summa);
		// Расчет если известна сумма налога или без ндфл
		if ( summa != 0 ) {
			result = summa / (precent * 100) * 100;
		} else {
			result = parseInt(notNdfl) + parseInt( notNdfl * (precent * 100) ) / ( 100 - (precent * 100) );
			console.log(result);
		}
		return result.toFixed(2);
	}

	function calcSumma() {
		result = ( ( parseInt( ndfl ) - valueChild ) * precent ).toFixed(2);

		if ( result < 0 ) result = 0;

		return  result;
	}

	function calcNotNdfl() {
		// если известна сумма налаго и ндфл
		return (ndfl - summa).toFixed(2);
	}
}

$(".form_ndfl input[name=tax]").change( function() {
	var ndfl 		= parseInt( $(".form_ndfl input[name=ndfl]").val() );

	if ( ndfl != "") {
		$(".form_ndfl").find("input[name=summa]").val( calc('summa') );
		$(".form_ndfl").find("input[name=notNdfl]").val( calc('notNdfl') );
	}
	
} );

$(".form_ndfl select[name=countChild]").change( function () {
	var ndfl 		= $(".form_ndfl input[name=ndfl]").val();
	var summa 		= $(".form_ndfl input[name=summa]").val();
	var notNdfl 	= $(".form_ndfl input[name=notNdfl]").val();

	if ( ndfl != 0 ) {
		//result = ( parseInt( ndfl ) - valueChild ) * precent; 

		$(".form_ndfl").find("input[name=summa]").val( calc('summa') );
		$(".form_ndfl").find("input[name=notNdfl]").val( calc('notNdfl') );
	} else if ( summa != 0 ) {
		result = ( summa / ( precent * 100 ) ) * 100;
		result = result;
		
		$(".form_ndfl").find("input[name=ndfl]").val( parseInt( result.toFixed(2) ) + valueChild );
		$(".form_ndfl").find("input[name=notNdfl]").val( ( result - summa ).toFixed(2) );
	} else if ( notNdfl != 0 ) {
		// сумма с ндфл = без ндфл + налог
		result = ( notNdfl * ( precent * 100 ) ) / ( 100 - ( precent * 100 ) );
		$(".form_ndfl").find("input[name=summa]").val( result.toFixed(2) );

		// налог = ндфл * %
		result = parseInt(result) + parseInt(notNdfl);
		$(".form_ndfl").find("input[name=ndfl]").val( result.toFixed(2) );
	} else if ( $(this).attr(name) == "countChild" ) {
		console.log("countChild");
	}
} );


$(".form_ndfl").submit(function (e) {
	e.stopPropagation();
	e.preventDefault();

	

	var precent 	= $(this).find("input[name=tax]:checked").val();
	precent = precent / 100;
	var countChild 	= $(this).find("select[name=countChild]").val();
	var valueChild 	= arr[countChild];
	var ndfl 		= $(this).find("input[name=ndfl]").val();
	var summa 		= $(this).find("input[name=summa]").val();
	var notNdfl 	= $(this).find("input[name=notNdfl]").val();
	var result 		= 0;

	if ( ndfl != 0) {
		result = ( parseInt( ndfl ) - valueChild ) * precent; 

		$(this).find("input[name=summa]").val( result );
		$(this).find("input[name=notNdfl]").val( ndfl - result );
	} else if ( summa != 0  ) {
		result = (summa / (precent * 100) ) * 100;
		result = result;
		
		$(this).find("input[name=ndfl]").val( result );
		$(this).find("input[name=notNdfl]").val( result - summa );
	} else if ( notNdfl != 0 ) {

		// сумма с ндфл = без ндфл + налог
		result = ( notNdfl * ( precent * 100 ) ) / ( 100 - ( precent * 100 ) );
		$(this).find("input[name=summa]").val( result );

		// налог = ндфл * %
		result = parseInt(result) + parseInt(notNdfl);
		$(this).find("input[name=ndfl]").val( result );
	}
});



	$( function() {

		$(".tab-pane form").submit(function (e) {
			e.stopPropagation();
			e.preventDefault();

			var digets = $(this).find("#form_ycn_digets");


		});

		$( "#count" ).slider({
			orientation: "horizontal",
			range: "min",
			max: 7,
			value: 0,
			slide: function( event, ui ) {
				$(".count__first").find(".count__digets__item").removeClass("active");
				$(".count__first .count__digets__item_first").addClass("active");
				if ( ui.value >= 7 ) {
					$(".count__first").find(".count__digets__item").addClass("active");
					$(".count__first").find("#count__first_input").val(250);
				} else if ( ui.value >= 6 ) {
					$(".count__first").find(".count__digets__item").addClass("active");
					$(".count__first").find(".count__digets__item_250").removeClass("active");
					$(".count__first").find("#count__first_input").val(200);
				} else if ( ui.value >= 5 ) {
					$(".count__first").find(".count__digets__item").addClass("active");
					$(".count__first").find(".count__digets__item_250").removeClass("active");
					$(".count__first").find(".count__digets__item_200").removeClass("active");
					$(".count__first").find("#count__first_input").val(150);
				} else if ( ui.value >= 4 ) {
					$(".count__first").find(".count__digets__item").addClass("active");
					$(".count__first").find(".count__digets__item_250").removeClass("active");
					$(".count__first").find(".count__digets__item_200").removeClass("active");
					$(".count__first").find(".count__digets__item_150").removeClass("active");
					$(".count__first").find("#count__first_input").val(100);
				} else if ( ui.value >= 3 ) {
					$(".count__first").find(".count__digets__item").addClass("active");
					$(".count__first").find(".count__digets__item_250").removeClass("active");
					$(".count__first").find(".count__digets__item_200").removeClass("active");
					$(".count__first").find(".count__digets__item_150").removeClass("active");
					$(".count__first").find(".count__digets__item_100").removeClass("active");
					$(".count__first").find("#count__first_input").val(50);
				} else if ( ui.value >= 2 ) {
					$(".count__first").find(".count__digets__item").addClass("active");
					$(".count__first").find(".count__digets__item_250").removeClass("active");
					$(".count__first").find(".count__digets__item_200").removeClass("active");
					$(".count__first").find(".count__digets__item_150").removeClass("active");
					$(".count__first").find(".count__digets__item_100").removeClass("active");
					$(".count__first").find(".count__digets__item_50").removeClass("active");
					$(".count__first").find("#count__first_input").val(30);
				} else if ( ui.value >= 1 ) {
					$(".count__first").find(".count__digets__item").addClass("active");
					$(".count__first").find(".count__digets__item_250").removeClass("active");
					$(".count__first").find(".count__digets__item_200").removeClass("active");
					$(".count__first").find(".count__digets__item_150").removeClass("active");
					$(".count__first").find(".count__digets__item_100").removeClass("active");
					$(".count__first").find(".count__digets__item_50").removeClass("active");
					$(".count__first").find(".count__digets__item_30").removeClass("active");
					$(".count__first").find("#count__first_input").val(10);
				}
			}
		});
	} );



	$( function() {
		$( "#count__people" ).slider({
			orientation: "horizontal",
			range: "min",
			max: 7,
			value: 0,
			slide: function( event, ui ) {
				$(".count__second").find(".count__digets__item").removeClass("active");
				$(".count__second .count__digets__item_first").addClass("active");
				if ( ui.value >= 7 ) {
					$(".count__second").find(".count__digets__item").addClass("active");
					$(".count__second").find("#count__second_input").val(70);
				} else if ( ui.value >= 6 ) {
					$(".count__second").find(".count__digets__item").addClass("active");
					$(".count__second").find(".count__digets__item_250").removeClass("active");
					$(".count__second").find("#count__second_input").val(60);
				} else if ( ui.value >= 5 ) {
					$(".count__second").find(".count__digets__item").addClass("active");
					$(".count__second").find(".count__digets__item_250").removeClass("active");
					$(".count__second").find(".count__digets__item_200").removeClass("active");
					$(".count__second").find("#count__second_input").val(50);
				} else if ( ui.value >= 4 ) {
					$(".count__second").find(".count__digets__item").addClass("active");
					$(".count__second").find(".count__digets__item_250").removeClass("active");
					$(".count__second").find(".count__digets__item_200").removeClass("active");
					$(".count__second").find(".count__digets__item_150").removeClass("active");
					$(".count__second").find("#count__second_input").val(40);
				} else if ( ui.value >= 3 ) {
					$(".count__second").find(".count__digets__item").addClass("active");
					$(".count__second").find(".count__digets__item_250").removeClass("active");
					$(".count__second").find(".count__digets__item_200").removeClass("active");
					$(".count__second").find(".count__digets__item_150").removeClass("active");
					$(".count__second").find(".count__digets__item_100").removeClass("active");
					$(".count__second").find("#count__second_input").val(30);
				} else if ( ui.value >= 2 ) {
					$(".count__second").find(".count__digets__item").addClass("active");
					$(".count__second").find(".count__digets__item_250").removeClass("active");
					$(".count__second").find(".count__digets__item_200").removeClass("active");
					$(".count__second").find(".count__digets__item_150").removeClass("active");
					$(".count__second").find(".count__digets__item_100").removeClass("active");
					$(".count__second").find(".count__digets__item_50").removeClass("active");
					$(".count__second").find("#count__second_input").val(20);
				} else if ( ui.value >= 1 ) {
					$(".count__second").find(".count__digets__item").addClass("active");
					$(".count__second").find(".count__digets__item_250").removeClass("active");
					$(".count__second").find(".count__digets__item_200").removeClass("active");
					$(".count__second").find(".count__digets__item_150").removeClass("active");
					$(".count__second").find(".count__digets__item_100").removeClass("active");
					$(".count__second").find(".count__digets__item_50").removeClass("active");
					$(".count__second").find(".count__digets__item_30").removeClass("active");
					$(".count__second").find("#count__second_input").val(10);
				}
			}
		});
	} );

	/*
	* $.post( "test.php", $( "form.calc" ).serialize(), function (data) {
		// ответ от сервера
	}, "json" );
	* */


	$("input[type=radio]").click(function(){
		//console.log( $(this).val() );
	});
	/* START  slider */
	//initialized
	var carousel = $("#carousel").featureCarousel({
		autoPlay:0,
		trackerIndividual:false,
		trackerSummation:false,
		smallFeatureWidth:.6,
		smallFeatureHeight:.6,
		topPadding:0,
		sidePadding:0,
		smallFeatureOffset:80,
		carouselSpeed: 500,
		leftButtonTag: '#carousel-left',
		rightButtonTag: '#carousel-right',
	});

	function initSlider(obj) {
		// определить количество слайдеров на странице
		var sliders = $(obj);
		var countSliders = sliders.length;

		// перебот слайдов в одном слайдере
		for ( var i = 0; i < countSliders; i++) {
			var deepSlides = $( sliders[i] ).find(".slider__item");
			var deepCountSlides = deepSlides.length;

			// ищим самый высокий слайд
			var heightSlider = 0;
			for( var j = 0; j < deepCountSlides; j++) {
				var heightNewSlide = $( deepSlides[j] ).outerHeight();
				console.log(heightNewSlide);
				if ( heightNewSlide > heightSlider )
					heightSlider = heightNewSlide;
			}

			// назначить высоту слайдере равной высоте самому высокому элементу внутри него
			$( sliders[i] ).height( heightSlider );
		}
	}
	/* END  slider */

	/* анимация блоков */
	new WOW().init({
		mobile: false
	});


	/* START preloader*/
	$("#cube-loader").css("display", "none");
	/* END preloader */

	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");

		if ( $(this).hasClass("active") ) {
			$(".navigation__content").addClass("active");
		} else {
			$(".navigation__content").removeClass("active");
		}
	});

	$(".btn_header").on("click", function () {
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");
		}
	});
	/* END откртие меню*/

	/* START красиво для hover в navigation */
	$(".navigation ul li a").hover(function () {
		var width = $(this).width();

		var elem = $(this);
		var offset = elem.offset().left - elem.parent().parent().offset().left;
		offset += parseInt( elem.css("padding-left") );

		$(".navigation__line").width( width );
		$(".navigation__line").css("left", offset);
		$(".navigation__line").css("bottom", "-6px");
	}, function () {
		// Забрать красоту! ВСЮ!
		var width = $(".navigation li.active a").width();
		var elem = $(".navigation li.active");
		var offset = elem.offset().left - elem.parent().offset().left;

		offset += parseInt( elem.css("padding-left") );
		$(".navigation__line").width( width );
		$(".navigation__line").css("left", offset);


        $(".navigation__line").css("bottom", 0);
	});
	/* END красиво для hover в navigation */

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			//$("header").addClass("bg-dark");
		}  else {
			//$("header").removeClass("bg-dark");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn_to_top").addClass("active");
		} else {
			$(".btn_to_top").removeClass("active");
		}

	});
	$('.btn_to_top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */










	$('#modalCalc').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) ;
		var recipient = button.data('whatever') ;
		//var modal = $(this);
		console.log(recipient);
		//modal.find('.modal-body .calc__val').val(recipient);
	})
	/* END calc event */





});


